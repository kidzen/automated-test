FROM php:fpm
LABEL maintainer="Rekamy"
WORKDIR /var/www

RUN apt-get update && apt-get install -y unzip git
RUN docker-php-ext-install pdo pdo_mysql
RUN cp .env.example .env
RUN composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
RUN php artisan key:generate
